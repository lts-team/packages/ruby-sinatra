ruby-sinatra (1.4.7-5+deb9u2) stretch-security; urgency=high

  * Non-maintainer upload by the ELTS security team.
  * CVE-2022-45442: Fix a reflected file download (RFD) attack in the
    Content-Disposition HTTP header which was being incorrectly derived from a
    potentially user-supplied filename. (Closes: #1025125)

 -- Utkarsh Gupta <utkarsh@debian.org>  Sun, 29 Jan 2023 05:12:02 +0530

ruby-sinatra (1.4.7-5+deb9u1) stretch; urgency=high

  * Non-maintainer upload by the ELTS security team.
  * CVE-2022-29970: Validate that any expanded paths match the allowed
    "public_dir" when serving static files. (Closes: #1014717)

 -- Chris Lamb <lamby@debian.org>  Tue, 12 Jul 2022 10:29:11 +0100

ruby-sinatra (1.4.7-5) unstable; urgency=medium

  * Team upload.
  * Drop build dependency on ruby-bluecloth

 -- Antonio Terceiro <terceiro@debian.org>  Tue, 15 Nov 2016 21:35:57 -0300

ruby-sinatra (1.4.7-4) unstable; urgency=medium

  * Team upload.
  * Drop bluecloth development/convenience dependency.
  * Bump standards version to 3.9.8

 -- Christian Hofstaedtler <zeha@debian.org>  Tue, 05 Jul 2016 15:50:59 +0200

ruby-sinatra (1.4.7-3) unstable; urgency=medium

  * Team upload.
  * Remove lintian-override from source dir:
    - Override warnings were already fixed

 -- Lucas Albuquerque Medeiros de Moura <lucas.moura128@gmail.com>  Sat, 05 Mar 2016 18:58:14 -0300

ruby-sinatra (1.4.7-2) unstable; urgency=medium

  * Reupload to unstable.

 -- Pirate Praveen <praveen@debian.org>  Fri, 04 Mar 2016 12:30:43 +0530

ruby-sinatra (1.4.7-1) experimental; urgency=medium

  [ Pirate Praveen ]

  * New upstream patch release
  * wrap-and-sort dependencies
  * Bump standards version to 3.9.7 (no changes)
  * Make copyright dep5 compliant

  [ Balasankar C ]
  * Add patch i18n-fix.patch
    - Specify available locales during testing (Closes: #808738)
  * Bump debhelper compatibility
  * Fix Vcs-* links to use secure protocol.

 -- Pirate Praveen <praveen@debian.org>  Thu, 25 Feb 2016 11:01:25 +0530

ruby-sinatra (1.4.6-2) unstable; urgency=medium

  * Team upload.

  [ Lucas Nussbaum ]
  * Remove myself from Uploaders.

  [ Pirate Praveen ]
  * Re-upload to unstable.
  * Add ruby-minitest, ruby-rabl, ruby-slim, ruby-bluecloth to build-depends.
    - now more tests are run during build.
  * Ignore require-rubygems failure.

 -- Pirate Praveen <praveen@debian.org>  Wed, 29 Apr 2015 18:45:41 +0530

ruby-sinatra (1.4.6-1) experimental; urgency=low

  * Team upload.
  * New upstream release.
  * Bump standards version to 3.9.6 (no changes)

 -- Pirate Praveen <praveen@debian.org>  Fri, 03 Apr 2015 15:31:40 +0530

ruby-sinatra (1.4.5-1) unstable; urgency=low

  * Team upload.
  * New upstream version: (Closes: #738387)
    - Drop obsolete patch.
    - Update path to Japanese documentation.
  * Get more test coverage by adding several Build-Depends.

 -- Jérémy Bobbio <lunar@debian.org>  Fri, 11 Apr 2014 00:39:29 +0200

ruby-sinatra (1.4.3-2) unstable; urgency=low

  Team upload.

  * Update Standards-Version to 3.9.5 (no changes)
  * Add alternative dependency on ruby-rack1.4
  * Fix test failure by cherry picking upstream fix (Closes: #728633)
  * Change ruby1.8 dependency to ruby

 -- Christian Hofstaedtler <zeha@debian.org>  Mon, 02 Dec 2013 20:56:50 +0100

ruby-sinatra (1.4.3-1) unstable; urgency=low

  [ Jérémy Bobbio ]
  * Team upload.
  * New upstream version.
  * Add git-buildpackage configuration.
  * Adjust Depends and Build-Depends.
  * Drop transitional packages now that Wheezy is stable.
  * Bump Standards-Version to version 3.9.4, no changes required.
  * Add missing Build-Depends on ruby-json.
  * Use rake to run the test suite to avoid false negative results.
  * Update paths to documentation.

  [ Cédric Boutillier ]
  * debian/control: remove obsolete DM-Upload-Allowed flag.
  * Use canonical URI in Vcs-* fields.

 -- Jérémy Bobbio <lunar@debian.org>  Wed, 07 Aug 2013 16:02:37 +0200

ruby-sinatra (1.3.2-2) unstable; urgency=low

  * Team upload
  * Add a dependency on ruby-rack-protection (Closes: #683496)

 -- Gunnar Wolf <gwolf@debian.org>  Sun, 05 Aug 2012 15:12:53 -0500

ruby-sinatra (1.3.2-1) unstable; urgency=low

  * New upstream version:1.3.2
  * Bump Standard Version: 3.9.3
  * Change transitional package priority to extra
  * Add lintian-overrides for transitional packages
  * Add rack-test-protection into Build-Depends 

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Fri, 29 Jun 2012 00:10:33 +0900

ruby-sinatra (1.2.6-1) unstable; urgency=low

  * New upstream version: 1.2.6
  * Add me to Uploaders
  * Add lintian-overrides

 -- Youhei SASAKI <uwabami@gfd-dennou.org>  Tue, 26 Jul 2011 01:23:37 +0900

ruby-sinatra (1.2.3-1) unstable; urgency=low

  * Switch to gem2deb-based packaging. Rename source and binary packages.

 -- Lucas Nussbaum <lucas@lucas-nussbaum.net>  Tue, 26 Apr 2011 15:41:55 +0200

libsinatra-ruby (1.0.really1.0-1) unstable; urgency=low

  [Laurent Vallar]
  * New upstream release.

  [Deepak Tripathi]
  * Added debian/source
  * Added debian/README.source
  
 -- Laurent Vallar <val@zbla.net>  Fri, 07 May 2010 15:21:21 +0200

libsinatra-ruby (1.0.b-1) unstable; urgency=low

  * New upstream release. No changes needed.

 -- Laurent Vallar <val@zbla.net>  Sat, 27 Mar 2010 16:37:27 +0100

libsinatra-ruby (1.0.a-1) unstable; urgency=low

  * New upstream release.
  * Bumped Standards-Version to 3.8.4. No changes needed.
  * Build ri documentation.
  * Port the package to Ruby 1.9.1.

 -- Laurent Vallar <val@zbla.net>  Mon, 22 Feb 2010 09:42:32 +0100

libsinatra-ruby (0.9.4-1) unstable; urgency=low

  * New uptstream release.
  * Remove patches associated to sinatra's lighthouseapp tickets #249 and
    #254, bugs are now fixed upstream.
  * Bumped Standards-Version to 3.8.3. No changes needed.

 -- Laurent Vallar <val@zbla.net>  Mon, 24 Aug 2009 16:46:00 +0200

libsinatra-ruby (0.9.3-1) unstable; urgency=low

  [ Laurent Vallar ]
  * Initial release (Closes: #534557).
  * Provide patch to fix :run default bad value on Sinatra applications,
    see https://sinatra.lighthouseapp.com/projects/9779-sinatra/tickets/249.
  * Provide patch to fix in file template support, see
    https://sinatra.lighthouseapp.com/projects/9779-sinatra/tickets/254
  * Move images from library path and provide patch to fix path in code.
  * Build HTML API documentation using rdoc instead of mislav-hanna gem which
    is not packaged for now.
  * Add graphviz dependency for build process plus many lintian fixes
    (thanks Ryan).

  [ Ryan Niebur ]
  * fix watch file

  [ Lucas Nussbaum ]
  * Fixed Vcs-* fields after pkg-ruby-extras SVN layout change.

 -- Laurent Vallar <val@zbla.net>  Wed, 24 Jun 2009 14:13:24 +0200
