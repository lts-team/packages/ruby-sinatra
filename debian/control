Source: ruby-sinatra
Section: ruby
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Youhei SASAKI <uwabami@gfd-dennou.org>,
           Pirate Praveen <praveen@debian.org>
Build-Depends: asciidoctor,
               debhelper (>= 9~),
               gem2deb,
               rake,
               ruby-builder,
               ruby-coffee-script,
               ruby-creole,
               ruby-erubis,
               ruby-haml,
               ruby-http,
               ruby-json,
               ruby-kramdown,
               ruby-liquid,
               ruby-maruku,
               ruby-minitest,
               ruby-nokogiri,
               ruby-rabl,
               ruby-rack (>= 1.4) | ruby-rack1.4,
               ruby-rack-protection (>= 1.4),
               ruby-rack-test,
               ruby-rdiscount,
               ruby-redcarpet,
               ruby-redcloth,
               ruby-sass,
               ruby-slim (>= 3.0~),
               ruby-tilt (>= 1.3.4),
               ruby-wikicloth,
               ruby-yajl,
               thin
Standards-Version: 3.9.8
Vcs-Git: https://anonscm.debian.org/git/pkg-ruby-extras/ruby-sinatra.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-ruby-extras/ruby-sinatra.git
Homepage: http://www.sinatrarb.com/
XS-Ruby-Versions: all

Package: ruby-sinatra
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ruby-rack (>= 1.4) | ruby-rack1.4,
         ruby-rack-protection (>= 1.4),
         ruby-tilt (>= 1.3.4),
         ${misc:Depends},
         ${shlibs:Depends}
Description: Ruby web-development dressed in a DSL
 Sinatra is an open source web framework for Ruby programming language.
 It provides simple Domain Specific Language (DSL) for defining RESTful
 HTTP actions, and then defining how the application is going to respond
 to them.
 .
 This framework is lighweight and uses Rack which is a web server
 interface developed to run many Ruby frameworks using the same stack.
